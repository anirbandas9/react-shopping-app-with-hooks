import React from "react";
import { Link } from "react-router-dom";
import StarRating from "./StarRating";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from "@fortawesome/free-regular-svg-icons";
import "../styles/Product.css";

function Product({ data, addToCart }) {
  const { title, image, price, rating, description } = data;
  const showDesc = description.split(" ").slice(0, 3).join(" ");
  const showTitle = title.split(" ").slice(0, 6).join(" ");
  return (
    <div className="product">
      <div className="product-img">
        <FontAwesomeIcon icon={faHeart} className="heart-icon" />
        <Link to={`/product/${data.id}`}>
          <img className="img" src={image} alt="" />
        </Link>
      </div>
      <div className="product-title">
        <h2>{showTitle}</h2>

        <p>${price}</p>
      </div>
      <div className="product-desc">{showDesc}</div>
      <div className="div-rating">
        <StarRating rating={rating.rate} maxRating={5} />
        <span className="rating-count">{`(${rating.count})`}</span>
      </div>
      <div className="product-btn">
        <button onClick={() => addToCart(data)} className="showDetailsBtn">
          Add to Cart
        </button>
      </div>
    </div>
  );
}

export default Product;
