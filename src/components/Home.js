import React, { useEffect, useState } from "react";
import ErrorPage from "./ErrorPage";
import Loader from "./Loader";
import Product from "./Product";
import FilterProducts from "./FilterProducts";
import Searchbar from "./SearchBar";
import SortByPrice from "./SortByPrice";

function Home({ addToCart }) {
  const [products, setProducts] = useState([]);
  const [originalProducts, setOriginalProducts] = useState([]);
  const [category, setCategory] = useState("All");
  const [sortProduct, setSortProduct] = useState("none");
  const [searchValue, setSearchValue] = useState("");
  const [loading, setLoading] = useState(true);
  const [pageError, setPageError] = useState(false);

  useEffect(() => {
    fetch("https://fakestoreapi.com/products")
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
        setOriginalProducts(data);
        setLoading(false);
      })
      .catch((err) => {
        setPageError(true);
        setLoading(false);
      });
  }, []);

  const handleCategoryChange = (e) => {
    const category = e.target.value;
    setCategory(category);
    if (category === "all") {
      setProducts(originalProducts);
    } else {
      const filteredProducts = originalProducts.filter((product) => {
        return product.category === category;
      });
      setProducts(filteredProducts);
    }
    setSortProduct("none");
    setSearchValue("");
  };

  const handleSearchChange = (e) => {
    const searchValue = e.target.value.toLowerCase();
    setSearchValue(searchValue);
    const filteredProducts = originalProducts.filter((product) => {
      return (
        product.title.toLowerCase().includes(searchValue) ||
        product.category.toLowerCase().includes(searchValue)
      );
    });
    setProducts(filteredProducts);
    setSortProduct("none");
    setCategory("all");
  };

  const handleSortProduct = (e) => {
    const sortProduct = e.target.value;
    setSortProduct(sortProduct);
    const sortedProducts = products.sort((productA, productB) => {
      if (sortProduct === "ascending") {
        return productA.price - productB.price;
      } else if (sortProduct === "descending") {
        return productB.price - productA.price;
      } else if (sortProduct === "rating-desc") {
        return productB.rating.rate - productA.rating.rate;
      } else if (sortProduct === "rating-asc") {
        return productA.rating.rate - productB.rating.rate;
      } else if (sortProduct === "atoz") {
        return productA.title.localeCompare(productB.title);
      } else if (sortProduct === "ztoa") {
        return productB.title.localeCompare(productA.title);
      } else {
        return products;
      }
    });
    setProducts(sortedProducts);
  };

  const ProductList = products.map((product) => {
    return <Product key={product.id} data={product} addToCart={addToCart}/>;
  });
  return (
    <>
      <h1 className="main-body-title">LootKart App</h1>
      <div className="filter-sort">
        <FilterProducts
          selectedCategory={category}
          onCategoryChange={handleCategoryChange}
        />
        <Searchbar
          searchValue={searchValue}
          onSearchChange={handleSearchChange}
        />
        <SortByPrice
          sortProduct={sortProduct}
          onSortProduct={handleSortProduct}
        />
      </div>
      {pageError ? (
        <ErrorPage />
      ) : loading ? (
        <Loader />
      ) : (
        <main>{ProductList}</main>
      )}
    </>
  );
}

export default Home;
