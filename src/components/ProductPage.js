import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Loader from "./Loader";
import ErrorPage from "./ErrorPage";
import StarRating from "./StarRating";
import "../styles/ProductPage.css";

function ProductPage({ addToCart }) {
  const { productId } = useParams();
  const [product, setProduct] = useState([]);
  const [loading, setLoading] = useState(true);
  const [pageError, setPageError] = useState(false);

  useEffect(() => {
    fetch(`https://fakestoreapi.com/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setProduct(data);
        setLoading(false);
      })
      .catch(() => {
        setPageError(true);
        setLoading(false);
      });
  }, [productId]);

  return (
    <div className="product-page">
      {pageError ? (
        <ErrorPage />
      ) : loading ? (
        <Loader />
      ) : (
        <div>
          <div className="product-img">
            <img className="img" src={product.image} alt="" />
          </div>
          <div className="product-title">
            <h2>{product.title}</h2>

            <p>${product.price}</p>
          </div>
          <div className="product-desc">{product.description}</div>
          <div className="product-rating">
            <StarRating rating={product.rating.rate} maxRating={5} />
            <span className="rating-count">{`(${product.rating.count})`}</span>
          </div>
          <div className="product-btn">
            <button onClick={() => addToCart(product)} className="showDetailsBtn">
              Add to Cart
            </button>
          </div>
        </div>
      )}
    </div>
  );
}

export default ProductPage;
