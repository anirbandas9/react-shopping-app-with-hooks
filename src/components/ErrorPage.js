import React from "react";

function ErrorPage() {
  return (
    <div className="pageError">
      <h1>404 DATA NOT FOUND</h1>
    </div>
  );
}

export default ErrorPage;