import React from 'react';
import "../styles/Cart.css"

function Cart({ cartItems, removeFromCart }) {
  const calculateTotal = () => {
    return cartItems.reduce((total, item) => total + item.price, 0);
  };

  return (
    <div className='cart'>
      <h2>Cart</h2>
      {cartItems.length === 0 ? (
        <p>Your cart is empty.</p>
      ) : (
        <div>
          <ul>
            {cartItems.map((item) => (
              <li key={item.productId} className='cart-item'>
                <img className='img' src={item.image} alt={item.title} />
                {item.title} - ${item.price}
                <button onClick={() => removeFromCart(item.productId)}>Remove</button>
              </li>
            ))}
          </ul>
          <p>Total: ${calculateTotal().toFixed(2)}</p>
        </div>
      )}
    </div>
  );
}

export default Cart;
