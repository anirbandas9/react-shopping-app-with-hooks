import React from "react";
import "../styles/SearchBar.css"

function Searchbar({ searchValue, onSearchChange }) {
  return (
    <div>
      <input
        type="search"
        name="searchbar"
        id="searchbar"
        className="search-input"
        onChange={onSearchChange}
        value={searchValue}
        placeholder="Search Products"
      />
    </div>
  );
}

export default Searchbar;
