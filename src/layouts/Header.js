import React from "react";
import logo from "../logo.svg";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser, faClipboard } from "@fortawesome/free-regular-svg-icons";
import "../styles/Header.css";

function Header() {
  const link = "#";
  return (
    <div className="header">
      <div className="header-logo">
        <img className="logo" src={logo} alt="" />
        <h2 className="header--title">LootKart</h2>
      </div>
      <ul className="nav-links">
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <a href={link}>Deals</a>
        </li>
        <li>
          <a href={link}>What's New</a>
        </li>
        <li>
          <a href={link}>Delivery</a>
        </li>
      </ul>
      <div className="header-account">
        <div>
          <FontAwesomeIcon icon={faUser} className="font-user" />
          <a className="account" href={link}>
            Account
          </a>
        </div>
        <div>
          <FontAwesomeIcon icon={faClipboard} className="font-user" />
          <Link className="account" to="/cart">
            Cart
          </Link>
        </div>
      </div>
    </div>
  );
}

export default Header;
