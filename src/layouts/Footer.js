import React from "react";
import logo from "../logo.svg";
import "../styles/Footer.css";

function Footer() {
  return (
    <footer className="footer">
      <img className="logo" src={logo} alt="" />
      <p>Copyright © 2023 Made by Anirban</p>
      <h2 className="header--title">LootKart</h2>
    </footer>
  );
}

export default Footer;
